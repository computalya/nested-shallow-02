# README

## short explanation

based on the demoapp [nested-shallow](https://gitlab.com/computalya/nested-shallow/)

add `sub tasks` to `Projects -> Tasks`

## scaffold _SubTask_

```bash
rails g scaffold SubTask name:string task:references
rails db:migrate
```

#### update `config/routes.rb`

> config/routes.rb

```ruby
  resources :projects do
    resources :tasks, shallow: true do
      resources :sub_tasks
    end
  end
```

#### update models

> app/models/task.rb

```ruby
class Task < ApplicationRecord
  belongs_to :project
  has_many   :sub_tasks, dependent: :destroy
end
```

> app/models/sub_task.rb

```ruby
class SubTask < ApplicationRecord
  belongs_to :task
end
```

seed database

> db/seeds.rb

```ruby
project1 = Project.create! name: 'Star Wars Project'
project2 = Project.create! name: 'Project Dagobah'

# create documents for each project
task1 = project1.tasks.create! name: 'task1 of Star Wars'
task2 = project1.tasks.create! name: 'task2 of Star Wars'

task3 = project2.tasks.create! name: 'task3 of Dagobah'
task4 = project2.tasks.create! name: 'task4 of Dagobah'

sub_task1 = task1.sub_tasks.create! name: 'sub_task 1'
sub_task2 = task2.sub_tasks.create! name: 'sub_task 2'
sub_task3 = task3.sub_tasks.create! name: 'sub_task 3'
sub_task4 = task4.sub_tasks.create! name: 'sub_task 4'
```

reset and seed database with new data

```bash
rails db:reset
```

#### update sub_tasks_controller

> app/controllers/sub_tasks_controller.rb

```ruby
class SubTasksController < ApplicationController
  before_action :set_task
  before_action :set_sub_task, only: [:show, :edit, :update, :destroy]

  # GET /sub_tasks
  # GET /sub_tasks.json
  def index
    @sub_tasks = @task.sub_tasks.all
  end

  # GET /sub_tasks/1
  # GET /sub_tasks/1.json
  def show
  end

  # GET /sub_tasks/new
  def new
    @sub_task = @task.sub_tasks.new
  end

  # GET /sub_tasks/1/edit
  def edit
  end

  # POST /sub_tasks
  # POST /sub_tasks.json
  def create
    @sub_task = @task.sub_tasks.new(sub_task_params)

    respond_to do |format|
      if @sub_task.save
        format.html { redirect_to @sub_task, notice: 'Sub task was successfully created.' }
        format.json { render :show, status: :created, location: @sub_task }
      else
        format.html { render :new }
        format.json { render json: @sub_task.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /sub_tasks/1
  # PATCH/PUT /sub_tasks/1.json
  def update
    respond_to do |format|
      if @sub_task.update(sub_task_params)
        format.html { redirect_to @sub_task, notice: 'Sub task was successfully updated.' }
        format.json { render :show, status: :ok, location: @sub_task }
      else
        format.html { render :edit }
        format.json { render json: @sub_task.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sub_tasks/1
  # DELETE /sub_tasks/1.json
  def destroy
    @sub_task.destroy
    respond_to do |format|
      format.html { redirect_to task_sub_tasks_url(@task), notice: 'Sub task was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_task
      @task = params.dig(:task_id) ? Task.find(params[:task_id]) : SubTask.find(params[:id]).task
    end

    def set_sub_task
      @sub_task = @task.sub_tasks.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def sub_task_params
      params.require(:sub_task).permit(:name, :task_id)
    end
end
```

#### update task views

> app/views/tasks/show.html.erb

```erb
<h5>
  <%= link_to 'SubTasks', task_sub_tasks_url(@task) %>
</h5>
```

#### update sub_task views

> app/views/sub_tasks/index.html.erb

```erb
<p id="notice"><%= notice %></p>

<h1>Sub Tasks</h1>

<table>
  <thead>
    <tr>
      <th>Name</th>
      <th>Task</th>
      <th colspan="3"></th>
    </tr>
  </thead>

  <tbody>
    <% @sub_tasks.each do |sub_task| %>
      <tr>
        <td><%= sub_task.name %></td>
        <td><%= sub_task.task.name %></td>
        <td><%= link_to 'Show', sub_task %></td>
        <td><%= link_to 'Edit', edit_sub_task_path(sub_task) %></td>
        <td><%= link_to 'Destroy', sub_task, method: :delete, data: { confirm: 'Are you sure?' } %></td>
      </tr>
    <% end %>
  </tbody>
</table>

<br>

<%= link_to 'New Sub Task', new_task_sub_task_path %>
```

> app/views/sub_tasks/\_form.html.erb

```erb
<%= form_with(model: sub_task,
              url: (@sub_task.new_record? ? [@task, sub_task] : sub_task_path),
              local: true) do |form| %>

  <% if sub_task.errors.any? %>
    <div id="error_explanation">
      <h2><%= pluralize(sub_task.errors.count, "error") %> prohibited this sub_task from being saved:</h2>

      <ul>
      <% sub_task.errors.full_messages.each do |message| %>
        <li><%= message %></li>
      <% end %>
      </ul>
    </div>
  <% end %>

  <%= form.label :name %>
  <%= form.text_field :name, autofocus: true %>
  

  <%= form.hidden_field :task_id, value: @task.id %>
    
  <div class="actions">
    <%= form.submit %>
  </div>
<% end %>
```

> app/views/sub_tasks/edit.html.erb

```erb 
<h1>Editing Sub Task</h1>

<%= render 'form', sub_task: @sub_task %>

<%= link_to 'Show', @sub_task %> |
<%= link_to 'Back', task_sub_tasks_url(@task) %>
```

> app/views/sub_tasks/new.html.erb

```erb 
<h1>New Sub Task</h1>

<%= render 'form', sub_task: @sub_task %>

<%= link_to 'Back', task_sub_tasks_path %>
```

> app/views/sub_tasks/show.html.erb

```erb 
<p id="notice"><%= notice %></p>

<p>
  <strong>Name:</strong>
  <%= @sub_task.name %>
</p>

<p>
  <strong>Task:</strong>
  <%= @task.name %>
</p>

<%= link_to 'Edit', edit_sub_task_path(@sub_task) %> |
<%= link_to 'Back', task_sub_tasks_url(@task) %>
```
