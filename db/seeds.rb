user  = User.create! email: 'user@example.com',  password: 'password', password_confirmation: 'password'
user.add_role :user

admin  = User.create! email: 'admin@example.com',  password: 'password', password_confirmation: 'password'
admin.remove_role :user
admin.add_role :admin

project1 = Project.create! name: 'Star Wars Project'
project2 = Project.create! name: 'Project Dagobah'

# create documents for each project
task1 = project1.tasks.create! name: 'task1 of Star Wars'
task2 = project1.tasks.create! name: 'task2 of Star Wars'

task3 = project2.tasks.create! name: 'task3 of Dagobah'
task4 = project2.tasks.create! name: 'task4 of Dagobah'

sub_task1 = task1.sub_tasks.create! name: 'sub_task 1'
sub_task2 = task2.sub_tasks.create! name: 'sub_task 2'
sub_task3 = task3.sub_tasks.create! name: 'sub_task 3'
sub_task4 = task4.sub_tasks.create! name: 'sub_task 4'

