require 'rails_helper'

RSpec.describe "sub_tasks/index", type: :view do
  before(:each) do
    assign(:sub_tasks, [
      SubTask.create!(
        :name => "Name",
        :task => nil
      ),
      SubTask.create!(
        :name => "Name",
        :task => nil
      )
    ])
  end

  it "renders a list of sub_tasks" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
