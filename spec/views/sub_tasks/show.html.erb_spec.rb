require 'rails_helper'

RSpec.describe "sub_tasks/show", type: :view do
  before(:each) do
    @sub_task = assign(:sub_task, SubTask.create!(
      :name => "Name",
      :task => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(//)
  end
end
