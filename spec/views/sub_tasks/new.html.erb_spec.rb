require 'rails_helper'

RSpec.describe "sub_tasks/new", type: :view do
  before(:each) do
    assign(:sub_task, SubTask.new(
      :name => "MyString",
      :task => nil
    ))
  end

  it "renders new sub_task form" do
    render

    assert_select "form[action=?][method=?]", sub_tasks_path, "post" do

      assert_select "input[name=?]", "sub_task[name]"

      assert_select "input[name=?]", "sub_task[task_id]"
    end
  end
end
