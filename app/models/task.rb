class Task < ApplicationRecord
  belongs_to :project
  has_many   :sub_tasks, dependent: :destroy
end
